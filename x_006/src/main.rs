use nannou::prelude::*;

// the window size is going to be 2x SIZE
// Useful when origin is in the middle
const SIZE: f32 = 400.0;

// Starting point of the app
fn main() {
    nannou::app(model).view(view).update(update).run();
}

// This struct represents our "data state"
// I'm not messing with a model on this simple app
// I gusss I could use a sketch, but I'm just going to keep doing apps
struct Model {}

// Builds the model
fn model(app: &App) -> Model {
    // The window is created here; we could
    // also create it in the main function
    app.new_window()
        .size(SIZE as u32 * 2, SIZE as u32 * 2)
        .key_pressed(key_pressed)
        .build()
        .unwrap();

    // We just return an empty struct here
    Model {}
}

// Updates the model (note the mutable reference)
fn update(_app: &App, _model: &mut Model, _update: Update) {}

// Draws on the screen
fn view(app: &App, _model: &Model, frame: Frame) {
    // We will use `draw` to draw on screen
    let draw = app.draw();

    // Let's first color the background
    draw.background().color(WHITE);

    // rotate the color based on what frame we are on
    let s_color = hsva(220.0 / 360.0, 1.0, 1.0, 0.009);

    // loop though the width. The extra complexity is to get floating point numbers 
    for q in 0..=SIZE as i32 * 2 * 100 {
        let i = q as f32 * 0.01;
        draw.quad()
            .stroke_color(s_color)
            .stroke_weight(1.0)
            .no_fill()
            .points(
                pt2(-SIZE / 10.0, -SIZE + i),
                pt2(-SIZE + i, SIZE / 7.0),
                pt2(SIZE, SIZE / 2.0 - i),
                pt2(SIZE /8.0 - i, -SIZE),
            )
            .rotate( (TAU / (app.elapsed_frames() as f32 * 0.0000001 )) * i);
    }

    // Eventually, we write our `draw` to frame
    draw.to_frame(app, &frame).unwrap();
}

// if a key is pressed, take a screenshot
fn key_pressed(app: &App, _model: &mut Model, _key: Key) {
    utilities::frame_capture(app, app.elapsed_frames());
}
