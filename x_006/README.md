# x_006
2022-08-04

![x_006.png](x_006.png)

This one builds a design similar to x_005, however it also spins it by a different amount each frame to generate different looking patters


### Running

```
$ cargo run --release
```
