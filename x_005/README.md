# x_005
2022-07-24

![x_005.png](x_005.png)

Replication of one of my p5.js sketches found [here](https://openprocessing.org/sketch/1559091) 


### Running

```
$ cargo run --release
```
