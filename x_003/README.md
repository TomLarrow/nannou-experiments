# x_003
2022-07-11

![x_003.png](x_003.png)

This is a continuation of the random walker from x_002. It starts all the walkers at the bottom of the canvas, colors them in relation to how far they are from the center of the screen, then turns them lose with an upwards bias

### Running

```
$ cargo run --release
```
