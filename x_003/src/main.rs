use nannou::prelude::*;

// Starting point of the app
fn main() {
    nannou::app(model).view(view).update(update).run();
}

struct Walker {
    current_position: Point2,
    previous_position: Point2,
    color: Srgba,
}

impl Walker {
    fn new(current_position: Point2, c: Srgba) -> Self {
        let previous_position = current_position.clone();
        let color = c;
        Walker {
            current_position,
            previous_position,
            color,
        }
    }
}

// This struct represents our "data state"
// It should contain and model whatever we want to draw on screen
struct Model {
    walkers: Vec<Walker>,
}

// Builds the model
fn model(app: &App) -> Model {
    // The window is created here; we could
    // also create it in the main function
    app.new_window()
        .size(800, 1600)
        .key_pressed(key_pressed)
        .build()
        .unwrap();

    let mut walkers = Vec::new();

    // Create 6,400 random walkers colored more blue the closer they are to 0
    // 1 - (abs(x) / 400) gives a float value between 0 and 1 along the x axis
    // the edges are closer to zero, and the middle closer to 1
    for i in -3200..3200 {
        let x = i as f32 / 8.0;
        let w = Walker::new(
            vec2(x, -800.0),
            srgba(0.0, 0.0, 1.0 - (abs(x) /400.0), 0.05)
        );
        walkers.push(w);
    }
    Model { walkers }
}

// Updates the model (note the mutable reference)
fn update(_app: &App, model: &mut Model, _update: Update) {
    for walker in &mut model.walkers {
        walker.previous_position = walker.current_position;
        // giving the Y coordinates a positive pull by having 4 positive, and only 2 negative
        walker.current_position =
            walker.previous_position + vec2(random_f32() * 10.0 - 5.0, random_f32() * 6.0 - 2.0);
    }
}

// Draws on the screen
fn view(app: &App, model: &Model, frame: Frame) {
    // We will use `draw` to draw on screen
    let draw = app.draw();

    // Let's first color the background, but only if we are on the first frame
    if app.elapsed_frames() == 1 {
        draw.background().color(WHITE);
    }

    for walker in &model.walkers {
        // Draw a line from the previous position to the current position
        draw.line()
            .start(walker.previous_position)
            .end(walker.current_position)
            .weight(2.0)
            .color(walker.color);
    }

    // Eventually, we write our `draw` to frame
    draw.to_frame(app, &frame).unwrap();
}

fn key_pressed(app: &App, _model: &mut Model, _key: Key) {
    utilities::frame_capture(app, app.elapsed_frames());
}
