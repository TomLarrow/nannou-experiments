# x_004
2022-07-16

![x_004.png](x_004.png)
![x_004_2.png](x_004_2.png)
![x_004_3.png](x_004_3.png)

Implementation of a random walker function. This time it only moves in the 4 cardinal directions, and draws a circle of radius 40 pixels after each movement.

Loosly inspired by [this sketch](https://openprocessing.org/sketch/1613347/) by [Lisa Sekaida](https://openprocessing.org/user/309939?view=sketches&o=48)

### Running

```
$ cargo run --release
```
