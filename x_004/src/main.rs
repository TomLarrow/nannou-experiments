use nannou::prelude::*;

const RADIUS: f32 = 40.0;

// Starting point of the app
fn main() {
    nannou::app(model).view(view).update(update).run();
}

struct Walker {
    current_position: Point2,
    color: Srgba,
}

impl Walker {
    fn new(current_position: Point2, c: Srgba) -> Self {
        let color = c;
        Walker {
            current_position,
            color,
        }
    }
}

// This struct represents our "data state"
// It should contain and model whatever we want to draw on screen
struct Model {
    walker: Walker,
}

// Builds the model
fn model(app: &App) -> Model {
    // The window is created here; we could
    // also create it in the main function
    app.new_window()
        .size(800, 800)
        .key_pressed(key_pressed)
        .build()
        .unwrap();

    // create 1 random colored walkers
    let walker = Walker::new(
        vec2(0.0, 0.0),
        srgba(random_f32(), random_f32(), random_f32(), 0.1),
    );
    Model { walker }
}

// Updates the model (note the mutable reference)
fn update(_app: &App, model: &mut Model, _update: Update) {
    // only take steps in size Radius so we need to generate the numbers 0-3
    // and move in the 4 directions accordingly
    let direction = random_range(0, 4);
    let d;

    match direction {
        0 => d = vec2(0.0, RADIUS),
        1 => d = vec2(RADIUS, 0.0),
        2 => d = vec2(0.0, -RADIUS),
        3 => d = vec2(-RADIUS, 0.0),
        _ => d = vec2(0.0, 0.0),
    }

    model.walker.current_position += d;

    // if it goes off screen, bring it back on
    if model.walker.current_position.x > 400.0 {
        model.walker.current_position.x -= 800.0
    }
    if model.walker.current_position.x < -400.0 {
        model.walker.current_position.x += 800.0
    }
    if model.walker.current_position.y > 400.0 {
        model.walker.current_position.y -= 800.0
    }
    if model.walker.current_position.y < -400.0 {
        model.walker.current_position.y += 800.0
    }
}

// Draws on the screen
fn view(app: &App, model: &Model, frame: Frame) {
    // We will use `draw` to draw on screen
    let draw = app.draw();

    // Let's first color the background, but only if we are on the first frame
    if app.elapsed_frames() == 1 {
        draw.background().color(WHITE);
    }

    draw.ellipse()
        .xy(model.walker.current_position)
        .stroke(BLACK)
        .stroke_weight(1.0)
        .radius(RADIUS)
        .color(model.walker.color);

    // Eventually, we write our `draw` to frame
    draw.to_frame(app, &frame).unwrap();
}

// if a key is pressed, take a screenshot
fn key_pressed(app: &App, _model: &mut Model, _key: Key) {
    utilities::frame_capture(app, app.elapsed_frames());
}
