use nannou::prelude::*;

// Starting point of the app
fn main() {
    nannou::app(model).view(view).update(update).run();
}

struct Walker {
    current_position: Point2,
    previous_position: Point2,
    color: Srgba,
}

impl Walker {
    fn new(current_position: Point2, c: Srgba) -> Self {
        let previous_position = current_position.clone();
        let color = c;
        Walker {
            current_position,
            previous_position,
            color,
        }
    }
}

// This struct represents our "data state"
// It should contain and model whatever we want to draw on screen
struct Model {
    walkers: Vec<Walker>,
}

// Builds the model
fn model(app: &App) -> Model {
    // The window is created here; we could
    // also create it in the main function
    app.new_window()
        .size(800, 800)
        .key_pressed(key_pressed)
        .build()
        .unwrap();

    let mut walkers = Vec::new();

    // Create 10,000 random colored walkers
    for _i in 1..10000 {
        let w = Walker::new(
            vec2(random_range(-400.0, 400.0), random_range(-400.0, 400.0)),
            srgba(random_f32(), random_f32(), random_f32(), 0.1),
        );
        walkers.push(w);
    }
    Model { walkers }
}

// Updates the model (note the mutable reference)
fn update(_app: &App, model: &mut Model, _update: Update) {
    for walker in &mut model.walkers {
        walker.previous_position = walker.current_position;
        walker.current_position =
            walker.previous_position + vec2(random_f32() * 10.0 - 5.0, random_f32() * 10.0 - 5.0);
    }
}

// Draws on the screen
fn view(app: &App, model: &Model, frame: Frame) {
    // We will use `draw` to draw on screen
    let draw = app.draw();

    // Let's first color the background, but only if we are on the first frame
    if app.elapsed_frames() == 1 {
        draw.background().color(WHITE);
    }

    for walker in &model.walkers {
        // Draw a line from the previous position to the current position
        draw.line()
            .start(walker.previous_position)
            .end(walker.current_position)
            .weight(2.0)
            .color(walker.color);
    }

    // Eventually, we write our `draw` to frame
    draw.to_frame(app, &frame).unwrap();
}

fn key_pressed(app: &App, _model: &mut Model, _key: Key) {
    utilities::frame_capture(app, app.elapsed_frames());
}
