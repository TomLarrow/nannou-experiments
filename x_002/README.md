# x_002
2022-07-11

![x_002.png](x_002.png)

Implementation of a random walker function. 10,000 random walkers, each with a random color and 10% alpha are turned lose to generate their random patterns.

### Running

```
$ cargo run --release
```
