use nannou::prelude::*;

    // Capture the frame!
    // Taken from https://github.com/nannou-org/nannou/blob/master/examples/draw/draw_capture.rs
pub fn frame_capture(app: &App, frame: u64){
    let file_path = captured_frame_path(app, frame);
    app.main_window().capture_frame(file_path);
}

// Taken from https://github.com/nannou-org/nannou/blob/master/examples/draw/draw_capture.rs
fn captured_frame_path(app: &App, frame: u64) -> std::path::PathBuf {
    // Create a path that we want to save this frame to.
    app.project_path()
        .expect("failed to locate `project_path`")
        // Capture all frames to the project directory
        .join(app.exe_name().unwrap())
        // append the folder name image_export
        .join("image_export")
        // Name each file after the number of the frame.
        .join(format!("{:04}", frame))
        // The extension will be PNG. We also support tiff, bmp, gif, jpeg, webp and some others.
        .with_extension("png")
}
