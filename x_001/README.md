# x_001
2022-06-18

![x_001.gif](x_001.gif)

A beginning animation in Nannou. It is just simple design crated by circles expanding and moving each frame, but I need to start somewhere. The code may be overly commented, but it is for my benefit, as much an anyone else's.

I'm not using the full power of Nannou models yet, so I could have used a Nannou sketch, but my goal is to take advantage of the model in the next experiment

### Running

```
$ cargo run --release
```
