use nannou::prelude::*;

// This struct represents our "data state"
// It should contain and model whatever we want to draw on screen
struct Model {}

// Builds the model
fn model(app: &App) -> Model {
    // The window is created here; we could
    // also create it in the main function
    app.new_window().size(200, 200).build().unwrap();

    // We just return an empty struct here
    Model {}
}

// Updates the model (note the mutable reference)
fn update(_app: &App, _model: &mut Model, _update: Update) {}

// Draws on the screen
fn view(app: &App, _model: &Model, frame: Frame) {
    // We will use `draw` to draw on screen
    let draw = app.draw();

    // define time in relation to elapsed frames
    let time = app.elapsed_frames() as f32;

    // Let's first color the background
    draw.background().color(BLACK);

    // draw 10 circles
    for i in 0..10 {
        // set an angle to 1/10th of a circle since we have 10 circles
        let angle = i as f32 * 0.1 * TAU;
        // then draw each circle outline using the angle to find the x/y of each one
        // we are adding time to each value to animate them.
        // This causes each circle to expand outwards away from origin
        // in the angle specified
        draw.ellipse()
            .x_y(time * angle.cos(), time * angle.sin())
            .stroke_color(WHITE)
            .stroke_weight(1.0)
            .no_fill();
    }

    // Capture frame to .png if the next line is uncommented
    // utilities::frame_capture(app, app.elapsed_frames());

    // Eventually, we write our `draw` to frame
    draw.to_frame(app, &frame).unwrap();
}

// Starting point of the app
fn main() {
    nannou::app(model).view(view).update(update).run();
}